import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "formatVersion"
})
export class FormatVersionPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    return Array.isArray(value)
      ? value.filter(v => v.length).join(", ")
      : value;
  }
}
