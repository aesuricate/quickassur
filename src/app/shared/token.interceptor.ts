import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from "@angular/common/http";
import { Observable } from 'rxjs';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const newrequest = request.clone({
      setHeaders: {
        'x-api-key': `E1TrrlfMCm5pNZEZ0zzT18t0poFyVf1P21TbGfyG`
      }
    });
    // console.log(newrequest, 'in interceptor');
    
    return next.handle(newrequest);
  }
}
