import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import { filter, switchMap } from 'rxjs/operators'
import { Observable } from 'rxjs';

@Component({
  selector: "app-car-tunnel",
  templateUrl: "./car-tunnel.component.html",
  styleUrls: ["./car-tunnel.component.scss"]
})
export class CarTunnelComponent implements OnInit {
  constructor(private fb: FormBuilder, private http: HttpClient) { }
  currentStep = 1;
  fuelTypes = ["Essence", "Gasoil"];
  fiscalPowers = [7, 8, 9, 10];
  carForm = this.fb.group({
    reInsure: true,
    newCar: "",
    searchBy: "",
    registrationNumber: [null, Validators.required],
    carProperties: this.fb.group({
      firstMEC: ["", Validators.required],
      brand: [null, Validators.required],
      model: [null, Validators.required],
      fuelType: [null, Validators.required],
      fiscalPower: [null, Validators.required],
      carVersion: [null, Validators.required]
    }),
    financing: [null, Validators.required]
  });

  brands$ = this.getCarInfo();

  models$ = this.mapFormToNetworkRessource(['carProperties', 'brand'], brand => this.getCarInfo( { brand }));

  versions$ = this.mapFormToNetworkRessource(['carProperties', 'model'], model => {
    const brand = this.carForm.value.carProperties.brand;
    return this.getCarInfo({ brand, model })
  });

  mapFormToNetworkRessource(formPath: string[], mapper: (val: string) => Observable<any[]>) {
    return this.carForm.get(formPath).valueChanges.pipe(
      filter((value: string) => !!value),
      switchMap(mapper));
  }
  getCarInfo(params = {}) {
    return this.http.get<string[]>('https://xcrfidrecl.execute-api.eu-west-3.amazonaws.com/default/QA-getBrands', { params })
  }

  ngOnInit() {
    this.carForm.valueChanges.subscribe(values => {
      this.scrollTo(".anchor:not(.hidden)");
    });
  }
  shouldShowStep(stepNumber: number) {
    return stepNumber <= this.currentStep;
  }
  setStep(step: number) {
    this.currentStep = step;
    this.scrollTo(".anchor");
  }
  scrollTo(selector: string): void {
    const elementList = document.querySelectorAll(selector);
    const element = elementList[elementList.length - 1] as HTMLElement;
    setTimeout(
      () =>
        element.scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest"
        }),
      0
    );
  }
}
