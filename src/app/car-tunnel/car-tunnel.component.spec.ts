import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarTunnelComponent } from './car-tunnel.component';

describe('CarTunnelComponent', () => {
  let component: CarTunnelComponent;
  let fixture: ComponentFixture<CarTunnelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarTunnelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarTunnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
